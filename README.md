# Letter charts for alphabet learning

<img height="400" src="doc/mrowka.png">

This project on the web: https://edugr.gitlab.io/letter-charts/ .

## Development Guide

1. When adding a new picture, make sure to add an appropriate entry to [pic-database.tex](./pic-database.tex). The only allowed license types are: Public Domain, `CC0`, `CC BY` and `CC BY-SA` (version `2.0`, `2.5`, `3.0` or `4.0`).
1. Pictures must be vertical rather than horizontal, in order to fit the page.
1. When adding a new word, make sure it is `6` characters or less.
1. All `.tex` files use `utf8` character encoding.

## Help

If you are new to TeX, start here: https://www.ctan.org/pkg/lshort .

If you are new to git, start here: https://git-scm.com/ .
