LCHARTSTEX=$(wildcard ./letter-charts-*.tex)
LCHARTSPDF_SANS=$(addsuffix -sans.pdf,$(basename $(LCHARTSTEX)))
LCHARTSPDF_SERIF=$(addsuffix -serif.pdf,$(basename $(LCHARTSTEX)))
LCHARTSPDF=$(LCHARTSPDF_SANS) $(LCHARTSPDF_SERIF)

# options to consider:
# pdflatex: -output-format pdf
TEX=xelatex
TEXOPTS=-halt-on-error -no-parse-first-line -no-shell-escape

.PHONY : default
default: help

.PHONY : help
help:
	@echo "Available targets:"
	@echo "    all          - generate all letter chart PDF files"
	@echo "    all-sans     - generate all letter chart PDF files (sans font family versions only)"
	@echo "    all-serif    - generate all letter chart PDF files (serif font family versions only)"
	@echo "    polski-serif - generate letter chart PDF for Polish, using serif font family"
	@echo "    clean        - delete temporary files"
	@echo "    debug        - print make variables"

define run-tex =
@echo "(1/2) Generating: $(1)"
@echo "            from: $(3)"
$(TEX) $(TEXOPTS) -jobname $(basename $(1)) "\def\lcfont{$(4)}\def\lcscale{$(5)}\input{$(2)}"
@echo "(2/2) Generating: $(1)"
@echo "            from: $(3)"
$(TEX) $(TEXOPTS) -jobname $(basename $(1)) "\def\lcfont{$(4)}\def\lcscale{$(5)}\input{$(2)}"
endef

$(LCHARTSPDF_SANS): %-sans.pdf: %.tex lettercharts.cls pic-database.tex
	$(call run-tex,$@,$<,$^,DejaVu Sans,9)

$(LCHARTSPDF_SERIF): %-serif.pdf: %.tex lettercharts.cls pic-database.tex
	$(call run-tex,$@,$<,$^,DejaVu Serif,8)

.PHONY : polski-serif
polski-serif : letter-charts-pl-serif.pdf

.PHONY : all-sans
all-sans : $(LCHARTSPDF_SANS)

.PHONY : all-serif
all-serif : $(LCHARTSPDF_SERIF)

.PHONY : all
all : $(LCHARTSPDF)

.PHONY : clean
clean:
	$(RM) *.aux *.dvi *.log *.out *.pdf

.PHONY : debug
debug:
	@echo "LCHARTSTEX:" $(LCHARTSTEX)
	@echo "LCHARTSPDF:" $(LCHARTSPDF)
	@echo "LCHARTSPDF_SANS:" $(LCHARTSPDF_SANS)
	@echo "LCHARTSPDF_SERIF:" $(LCHARTSPDF_SERIF)

