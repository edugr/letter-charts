\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lettercharts}[2023/11/27 Letter Charts]

% Define a class option for changing the paper size
\DeclareOption{letterpaper}{\PassOptionsToClass{letterpaper}{article}}
\DeclareOption{a4paper}{\PassOptionsToClass{a4paper}{article}}

% Pass every unknown option to the article class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

% Provide the default option list
% TODO: this doesn't seem to work, but why?!
% \ExecuteOptions{a4paper,12pt}

% Process the options
\ProcessOptions\relax

% Load the article class
\LoadClassWithOptions{article}
\pagestyle{empty}

% Load the remaining packages and set their options
\RequirePackage[OT4]{fontenc}
\RequirePackage[lmargin=2cm,rmargin=2cm,tmargin=1.5cm,bmargin=6.5cm]{geometry}
\RequirePackage{tcolorbox}
\RequirePackage{graphicx}
\RequirePackage{fontspec}
\RequirePackage{datatool}

% load the package in the preamble, at the end of all the other packages but prior to other settings
\RequirePackage[xetex]{hyperref}
\hypersetup{pdfborder=0 0 0, colorlinks=true, linkcolor=blue, urlcolor=blue}
\RequirePackage{bookmark}

% A command for highlighting the text (using the red color)
\newcommand{\lcmark}[1]{\textcolor{red}{#1}}%

% List of letter card minipages with license information
\newcommand{\lccardlist}{}%

% TODO this approach does not currently work, apparently due to a texlive bug: https://tex.stackexchange.com/questions/696361/how-do-i-search-for-a-substring-and-highlight-the-occurrences#comment1729091_696361
%
% Highlights all occurences of the given search string within the given text.
% Parameters are:
%
% 1. the text
% 2. the search string within the text
% \newcommand{\lcmarkall}[2]{\StrSubstitute{#1}{#2}{\lcmark{#2}}

% Draws a single letter card. Parameters are:
%
% 1. the letter in question
% 2. image caption
% 3. image filename
\newcommand{\lccard}[3]{%
% letter card minipage
\begin{minipage}[t]{0.95\linewidth}%
\begin{tcolorbox}[standard jigsaw,notitle,titlebox=invisible,colframe=black,opacityframe=1,opacityback=0,arc=0mm]%
\centering%
\vspace{0.5cm}%
\scalebox{\lcscale}{\fontspec{\lcfont}#1}\\%
\vspace{1cm}%
\includegraphics[height=.6\textheight,keepaspectratio]{pic/#3}\\%
\vspace{1cm}%
% TODO: this approach does not currently work (see above)
% \scalebox{5}{\lcmarkall{#2}{#1}}\\%
\scalebox{\lcscale}{\fontspec{\lcfont}#2}%
\vspace{0.5cm}%
\end{tcolorbox}%
\end{minipage}%
\newpage%
% Lookup the image from the database by the filename and extend the card list
\def\cardentry{%
\DTLgetrowindex{\myrowidx}{picdb}{\dtlcolumnindex{picdb}{Filename}}{#3}%
\dtlgetrow{picdb}{\myrowidx}%
\dtlgetentryfromcurrentrow{\mysrc}{2}%
\dtlgetentryfromcurrentrow{\myauthor}{3}%
\dtlgetentryfromcurrentrow{\mychg}{4}%
\dtlgetentryfromcurrentrow{\mylic}{5}%
\dtlgetentryfromcurrentrow{\mylicurl}{6}%
\item%
\begin{minipage}[t]{0.95\linewidth}%
\includegraphics[height=.1\textheight,keepaspectratio]{pic/#3}\\%
Source: \url{\mysrc}\\%
\ifx\myauthor\empty%
Author: unknown\\%
\else%
Author: \myauthor\\%
\fi%
\ifx\mychg\empty%
Changes: none\\%
\else%
Changes: \mychg\\%
\fi%
License: \mylic\\%
\ifx\mylicurl\empty%
\else%
License Link: \url{\mylicurl}\\%
\fi%
\end{minipage}}%
\expandafter\g@addto@macro\expandafter\lccardlist\expandafter{\cardentry}%
}%

% Image credits and licensing information
\newcommand{\lcimagecredits}[0]{%
\section*{Image Credits}%
\label{image-credits}%
\begin{enumerate}%
\lccardlist%
\end{enumerate}%
}%

% Pre-set some document defaults
\setlength{\parindent}{0pt}
\setlength{\parskip}{7.2pt}

\author{Stanisław Findeisen and Others}

% Begin Document hook
\AtBeginDocument{%
\begin{fussy}%
\maketitle%
\section*{Terms of use}
Copyright~\copyright~2023 Stanisław Findeisen, Image Authors (see Image Credits at the end of this document, on page \pageref{image-credits}) and other Contributors (see git history). All rights reserved.

This document is free; you can redistribute it and/or modify it under the terms of the \href{https://creativecommons.org/licenses/by/4.0/}{Attribution-ShareAlike 4.0 International License} as published by Creative Commons.

\includegraphics[width=.2\textwidth,keepaspectratio]{cc-by-sa.png}

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

This document is distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchantabilit or fitness for a particular purpose.

\section*{Welcome!}
Thank you for choosing this book! Check our website: \url{https://edugr.gitlab.io/letter-charts/} for the latest version. Enjoy!

\thispagestyle{empty}%
\begin{center}%
}

% End Document hook
\AtEndDocument{%
\end{center}%
\lcimagecredits
\end{fussy}%
}

% Load picture database
\input{pic-database.tex}
