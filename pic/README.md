# Picture sources and credits

For the list of all pictures along with their origins and licensing information, see [pic-database.tex](./../pic-database.tex). Picture credits are also to be found at the end of each PDF output file which uses them.
